const ipcRenderer = require("electron").ipcRenderer;

$(document).ready(() => {
  registerInputHandlers();
  $("#toast").toast({ delay: 5000 });
  $("#limit").val(1000);
});

const registerInputHandlers = () => {
  $("#chooseFolder").click(() => {
    ipcRenderer.send("selectFolder", {});
  });

  $("#export").click(() => {
    let token = $("#token").val();
    let folder = $("#folder").val();
    let limit = $("#limit").val();
    ipcRenderer.send("export", { token, folder, limit });
    $("#toastContent").removeClass("alert-danger alert-success");
    $("#toastContent").addClass("alert-info");
    $("#toastContent").text("Starting export...");
    $("#toast").toast("show");
  });
};

ipcRenderer.on("folderSelected", (event, args) => {
  $("#folder").val(args.folder);
});

ipcRenderer.on("exportCompleted", (event, args) => {
  $("#toastContent").removeClass("alert-danger alert-info");
  $("#toastContent").addClass("alert-success");
  $("#toastContent").text(`Wrote export to ${args.file}`);
});

ipcRenderer.on("exportFailed", (event, args) => {
  console.log(args);
  $("#toastContent").removeClass("alert-success alert-info");
  $("#toastContent").addClass("alert-danger");
  $("#toastContent").text(args.message);
});
